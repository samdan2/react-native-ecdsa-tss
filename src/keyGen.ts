// P1KeyGen
export interface IP1KeyShare {
  x1: string;
  public_key: string;
  paillier_private_key: string;
  paillier_public_key: string;
}

export interface INativeP1KeyGenResult {
  p1KeyGen: string;
  msg_to_send: string | null;
  p1_key_share: string | null;
}

export interface IP1KeyGenResult {
  p1KeyGen: IP1KeyGen;
  msg_to_send: string | null;
  p1_key_share: IP1KeyShare | null;
}

enum P1KeyGenState {
  COMPLETE = 0,
  FAILED = 1,
  NOT_INITIALIZED = 2,
  CREATE_KEY_GEN_MSG_1 = 3,
  PROCESS_KEY_GEN_MSG_2 = 4,
}

export interface IP1KeyGen {
  session_id: string;
  x1: string;
  paillierPublicKey?: any;
  paillierPrivateKey?: any;
  q1: any | null;
  dLogProof1?: any | null;
  blindFactor1?: string | null;
  eph1: any | null;
  e1: any | null;
  dLogProof2: any | null;
  blindFactor2?: string | null;
  expectedPublicKey?: any;
  state: P1KeyGenState;
}

export interface INativeP1KeyGenMessage1Result {
  p1KeyGen: string;
  message1: string;
}

export interface IP1KeyGenMessage1Result {
  p1KeyGen: IP1KeyGen;
  message1: string;
}

// P2KeyGen
export enum P2KeyGenState {
  COMPLETE = 0,
  FAILED = 1,
  PROCESS_KEY_GEN_MSG_1 = 2,
  PROCESS_KEY_GEN_MSG_3 = 3,
}

export interface IP2KeyShare {
  x2: string;
  public_key: string;
  c_key_x1: string;
  paillier_public_key: string;
}

export interface IP2KeyGenResult {
  p2KeyGen: IP2KeyGen;
  msg_to_send: string | null;
  p2_key_share: IP2KeyShare | null;
}

export interface INativeP2KeyGenResult {
  p2KeyGen: string;
  msg_to_send: string | null;
  p2_key_share: string | null;
}

export interface IP2KeyGen {
  session_id: string;
  x2: bigint;
  eph2: bigint | any;
  commitment1: string | any;
  commitment2: string | any;
  expectedPublicKey?: string;
  state: P2KeyGenState;
}
