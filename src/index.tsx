import base64Decode from 'fast-base64-decode';
import base64Encode from 'fast-base64-encode';
import { NativeModules, Platform } from 'react-native';
import type {
  INativeP1KeyGenMessage1Result,
  INativeP1KeyGenResult,
  INativeP2KeyGenResult,
  IP1KeyGen,
  IP1KeyGenMessage1Result,
  IP1KeyGenResult,
  IP2KeyGen,
  IP2KeyGenResult,
} from './keyGen';
import type {
  NativeP1SignMessageResult,
  NativeP1SignProcessMessageResult,
  NativeP2SignProcessMessageResult,
  P1Signature,
  P1SignMessageResult,
  P1SignProcessMessageResult,
  P2Signature,
  P2SignProcessMessageResult,
} from './signature';

const LINKING_ERROR =
  `The package 'react-native-ecdsa-tss' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const EcdsaTss = NativeModules.EcdsaTss
  ? NativeModules.EcdsaTss
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export {
  INativeP1KeyGenMessage1Result,
  INativeP1KeyGenResult,
  INativeP2KeyGenResult,
  IP1KeyGen,
  IP1KeyGenMessage1Result,
  IP1KeyGenResult,
  IP2KeyGen,
  IP2KeyGenResult,
  NativeP1SignMessageResult,
  NativeP1SignProcessMessageResult,
  NativeP2SignProcessMessageResult,
  P1Signature,
  P1SignMessageResult,
  P1SignProcessMessageResult,
  P2Signature,
  P2SignProcessMessageResult,
};

export async function generateP1KeyGen(
  sessionId: string,
  x1?: Uint8Array | null
): Promise<IP1KeyGen> {
  const p1KeyGenX1 = x1 ? base64Encode(x1) : null;
  return JSON.parse(await EcdsaTss.generateP1KeyGen(sessionId, p1KeyGenX1));
}

export async function p1KeyGenFromJSON(
  p1KeyGenJSON: string
): Promise<IP1KeyGen> {
  return JSON.parse(await EcdsaTss.p1KeyGenFromJSON(p1KeyGenJSON));
}

export async function getP1KeyGenMessage1(
  p1KeyGenJSON: string
): Promise<IP1KeyGenMessage1Result> {
  const nativeMessage1Result: INativeP1KeyGenMessage1Result =
    await EcdsaTss.getP1KeyGenMessage1(p1KeyGenJSON);
  return {
    message1: nativeMessage1Result.message1,
    p1KeyGen: JSON.parse(nativeMessage1Result.p1KeyGen),
  };
}

export async function p1KeyGenProcessMessage(
  p1KeyGenJSON: string,
  messageFromParty2: string
): Promise<IP1KeyGenResult> {
  const nativeP1KeyGenResult: INativeP1KeyGenResult =
    await EcdsaTss.p1KeyGenProcessMessage(p1KeyGenJSON, messageFromParty2);
  return {
    p1KeyGen: JSON.parse(nativeP1KeyGenResult.p1KeyGen),
    msg_to_send: nativeP1KeyGenResult.msg_to_send,
    p1_key_share: nativeP1KeyGenResult?.p1_key_share
      ? JSON.parse(nativeP1KeyGenResult.p1_key_share)
      : null,
  };
}

export async function generateP2KeyGen(
  sessionId: string,
  x2?: Uint8Array | null
): Promise<IP2KeyGen> {
  const p2KeyGenX2 = x2 ? base64Encode(x2) : null;
  return JSON.parse(await EcdsaTss.generateP2KeyGen(sessionId, p2KeyGenX2));
}

export async function p2KeyGenFromJSON(
  p2KeyGenJSON: string
): Promise<IP2KeyGen> {
  return JSON.parse(await EcdsaTss.p2KeyGenFromJSON(p2KeyGenJSON));
}

export async function p2KeyGenProcessMessage(
  p2KeyGenJSON: string,
  msgFromParty1: string
): Promise<IP2KeyGenResult> {
  const nativeP2KeyGenResult: INativeP2KeyGenResult =
    await EcdsaTss.p2KeyGenProcessMessage(p2KeyGenJSON, msgFromParty1);

  return {
    p2KeyGen: JSON.parse(nativeP2KeyGenResult.p2KeyGen),
    p2_key_share: nativeP2KeyGenResult?.p2_key_share
      ? JSON.parse(nativeP2KeyGenResult.p2_key_share)
      : null,
    msg_to_send: nativeP2KeyGenResult.msg_to_send,
  };
}

export async function randBytes(byteLength: number): Promise<Uint8Array> {
  const array = new Uint8Array(byteLength);
  const randValue = await EcdsaTss.getRandomBase64(byteLength);
  base64Decode(randValue, array);
  return array;
}

// sign process
// p1
export async function generateP1Signature(
  sessionId: string,
  messageHash: Uint8Array,
  p1KeyShareJSON: string
): Promise<P1Signature> {
  return JSON.parse(
    await EcdsaTss.generateP1Signature(
      sessionId,
      base64Encode(messageHash),
      p1KeyShareJSON
    )
  );
}

export async function p1SignatureFromJSON(
  p1SignJSON: string
): Promise<P1Signature> {
  return JSON.parse(await EcdsaTss.p1SignatureFromJSON(p1SignJSON));
}

export async function getP1SignatureMessage1(
  p1SignJSON: string
): Promise<P1SignMessageResult> {
  const nativeMessage1Result: NativeP1SignMessageResult =
    await EcdsaTss.getP1SignatureMessage1(p1SignJSON);
  return {
    message1: nativeMessage1Result.message1,
    p1Sign: JSON.parse(nativeMessage1Result.p1Sign),
  };
  // return {
  //   message1: nativeMessage1Result.message1,
  //   p1KeyGen: JSON.parse(nativeMessage1Result.p1KeyGen),
  // };
}

export async function p1SignatureProcessMessage(
  p1SignJSON: string,
  msgFromParty2: string
): Promise<P1SignProcessMessageResult> {
  const result: NativeP1SignProcessMessageResult =
    await EcdsaTss.p1SignatureProcessMessage(p1SignJSON, msgFromParty2);

  return {
    p1Sign: JSON.parse(result.p1Sign),
    msg_to_send: result.msg_to_send,
    signature: result.signature,
  };
}

// p2
export async function generateP2Signature(
  sessionId: string,
  messageHash: Uint8Array,
  p2KeyShareJSON: string
): Promise<P2Signature> {
  return JSON.parse(
    await EcdsaTss.generateP2Signature(
      sessionId,
      base64Encode(messageHash),
      p2KeyShareJSON
    )
  );
}

export async function p2SignatureFromJSON(
  p2SignJSON: string
): Promise<P1Signature> {
  return JSON.parse(await EcdsaTss.p2SignatureFromJSON(p2SignJSON));
}

export async function p2SignatureProcessMessage(
  p2SignJSON: string,
  msgFromParty1: string
): Promise<P2SignProcessMessageResult> {
  const result: NativeP2SignProcessMessageResult =
    await EcdsaTss.p2SignatureProcessMessage(p2SignJSON, msgFromParty1);

  return {
    p2Sign: JSON.parse(result.p2Sign),
    msg_to_send: result.msg_to_send,
    signature: result.signature,
  };
}

// Key refresh
export async function p1GetInstanceForKeyRefresh(
  sessionId: string,
  oldP1KeyShareJSON: string
): Promise<IP1KeyGen> {
  return JSON.parse(
    await EcdsaTss.p1GetInstanceForKeyRefresh(sessionId, oldP1KeyShareJSON)
  );
}
export async function p2GetInstanceForKeyRefresh(
  sessionId: string,
  oldP2KeyShareJSON: string
): Promise<IP2KeyGen> {
  return JSON.parse(
    await EcdsaTss.p2GetInstanceForKeyRefresh(sessionId, oldP2KeyShareJSON)
  );
}
