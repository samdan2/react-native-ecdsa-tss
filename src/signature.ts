export interface P1KeyShare {
  paillier_private_key: Paillierprivatekey;
  paillier_public_key: string;
  public_key: string;
  x1: string;
}

export interface Paillierprivatekey {
  p: string;
  q: string;
}

export interface P1Signature {
  blindFactor?: any;
  dLogProof?: any;
  k1?: any;
  messageHash: string;
  p1KeyShare: P1KeyShare;
  r?: any;
  r1?: any;
  session_id: string;
  state: string;
}

// p2 interface
export interface P2KeyShare {
  c_key_x1: string;
  paillier_public_key: string;
  public_key: string;
  x2: string;
}

export interface P2Signature {
  commitment?: any;
  k2?: any;
  messageHash: string;
  p2KeyShare?: P2KeyShare;
  session_id: string;
  state: string;
}

export interface P1SignMessageResult {
  message1: string;
  p1Sign: P1Signature;
}

export interface NativeP1SignMessageResult {
  message1: string;
  p1Sign: string;
}

export interface NativeP2SignProcessMessageResult {
  msg_to_send?: string;
  signature?: string;
  p2Sign: string;
}

export interface P2SignProcessMessageResult {
  msg_to_send?: string;
  signature?: string;
  p2Sign: P2Signature;
}

export interface NativeP1SignProcessMessageResult {
  msg_to_send?: string;
  signature?: string;
  p1Sign: string;
}

export interface P1SignProcessMessageResult {
  msg_to_send?: string;
  signature?: string;
  p1Sign: P2Signature;
}
