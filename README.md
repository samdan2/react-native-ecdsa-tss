# ECDSA secp256k1 TSS(2,2) React Native library

## Actions

The library provides the following actions:

- [x] key generation
- [x] key signin
- [x] Key share refresh

## Benchmarking and Performance

Windows 11 Android studio emulator (total calculation time of two parties)

```
KeyGen        300-600 ms
Sign          60-80   ms
Key refresh   300-600 ms
```

## Usage

An example of KeyGen (from example/App.jsx):

```js
const keygen = React.useCallback(async (): Promise<KeygenPair> => {
  try {
    const SESSION_ID = 'keygen_session_id';
    const x1 = await randBytes(32);
    const x2 = await randBytes(32);

    const p1KeyGen = await generateP1KeyGen(SESSION_ID, x1);
    let p1KeyGenStorage: string = JSON.stringify(p1KeyGen);

    const p2KeyGen = await generateP2KeyGen(SESSION_ID, x2);
    let p2KeyGenStorage: string = JSON.stringify(p2KeyGen);

    let p1KeyShare;
    let p2KeyShare;
    let p1KeyGenCompleted = false;
    let p2KeyGenCompleted = false;
    let messageFromParty1 = null;
    let messageFromParty2 = null;

    let tempP1KeyGen: IP1KeyGen;
    let tempP2KeyGen: IP2KeyGen;

    tempP1KeyGen = await p1KeyGenFromJSON(p1KeyGenStorage);
    const message1Result = await getP1KeyGenMessage1(
      JSON.stringify(tempP1KeyGen)
    );
    messageFromParty1 = message1Result.message1;
    p1KeyGenStorage = JSON.stringify(message1Result.p1KeyGen);

    console.log('Silentshard', messageFromParty1);
    for (let i = 0; i < 2; i++) {
      if (!p2KeyGenCompleted) {
        tempP2KeyGen = await p2KeyGenFromJSON(p2KeyGenStorage);
        const p2KeyGenResult: IP2KeyGenResult = await p2KeyGenProcessMessage(
          JSON.stringify(tempP2KeyGen),
          messageFromParty1
        );
        p2KeyGenStorage = JSON.stringify(p2KeyGenResult.p2KeyGen);

        if (p2KeyGenResult.msg_to_send) {
          messageFromParty2 = p2KeyGenResult.msg_to_send;
          console.log('Silentshard', messageFromParty2);
        }
        if (p2KeyGenResult.p2_key_share) {
          p2KeyShare = p2KeyGenResult.p2_key_share;
          p2KeyGenCompleted = true;
        }
      }

      if (!p1KeyGenCompleted) {
        tempP1KeyGen = await p1KeyGenFromJSON(p1KeyGenStorage);
        const p1KeyGenResult = await p1KeyGenProcessMessage(
          JSON.stringify(tempP1KeyGen),
          messageFromParty2
        );

        p1KeyGenStorage = JSON.stringify(p1KeyGenResult.p1KeyGen);
        if (p1KeyGenResult.msg_to_send) {
          messageFromParty1 = p1KeyGenResult.msg_to_send;
          console.log('Silentshard', messageFromParty1);
        }
        if (p1KeyGenResult.p1_key_share) {
          p1KeyShare = p1KeyGenResult.p1_key_share;
          p1KeyGenCompleted = true;
        }
      }
    }
    if (!(p1KeyShare && p2KeyShare)) {
      throw new Error('Keygen failed');
    }
    return { p1KeyShare, p2KeyShare };
  } catch (e) {
    console.log(e);
    throw e;
  }
}, []);
```

An example of sign (from example/App.jsx):

```js
const sign = React.useCallback(
  async (
    messageHash: Uint8Array,
    p1KeyShare: any,
    p2KeyShare: any
  ): Promise<string> => {
    const sessionId = 'sign_session_id';

    const p1Signature = await generateP1Signature(
      sessionId,
      messageHash,
      JSON.stringify(p1KeyShare)
    );
    const p2Signature = await generateP2Signature(
      sessionId,
      messageHash,
      JSON.stringify(p2KeyShare)
    );

    let p1SignatureStorage = JSON.stringify(p1Signature);
    let p2SignatureStorage = JSON.stringify(p2Signature);

    let s1: string | null = null;
    let s2: string | null = null;
    let p1SignCompleted = false;
    let p2SignCompleted = false;

    let msgFromParty1 = null;
    let msgFromParty2 = null;

    let tempP1Sign: P1Signature;
    let tempP2Sign: P2Signature;

    tempP1Sign = await p1SignatureFromJSON(p1SignatureStorage);
    const message1Result = await getP1SignatureMessage1(
      JSON.stringify(tempP1Sign)
    );
    msgFromParty1 = message1Result.message1;
    p1SignatureStorage = JSON.stringify(message1Result.p1Sign);

    for (let i = 0; i < 3; i++) {
      if (!p2SignCompleted) {
        tempP2Sign = await p2SignatureFromJSON(p2SignatureStorage);
        const pmResult: P2SignProcessMessageResult =
          await p2SignatureProcessMessage(
            JSON.stringify(tempP2Sign),
            msgFromParty1
          );
        p2SignatureStorage = JSON.stringify(pmResult.p2Sign);
        if (pmResult.msg_to_send) {
          msgFromParty2 = pmResult.msg_to_send;
        }
        if (pmResult.signature) {
          s2 = pmResult.signature;
          p2SignCompleted = true;
        }
      }

      if (msgFromParty2 && !p1SignCompleted) {
        tempP1Sign = await p1SignatureFromJSON(p1SignatureStorage);
        const pmResult: P1SignProcessMessageResult =
          await p1SignatureProcessMessage(
            JSON.stringify(tempP1Sign),
            msgFromParty2
          );
        p1SignatureStorage = JSON.stringify(pmResult.p1Sign);

        if (pmResult.msg_to_send) {
          msgFromParty1 = pmResult.msg_to_send;
        }
        if (pmResult.signature) {
          s1 = pmResult.signature;
          p1SignCompleted = true;
        }
      }
    }

    if (s1 == null) {
      throw new Error('s1 is null');
    }
    if (s2 == null) {
      throw new Error('s2 is null');
    }
    if (s1 !== s2) {
      throw new Error('Signatures not match');
    }
    return s1;
  },
  []
);
```

An example of keyRefresh (from example/App.jsx):

```js
const keyRefresh = React.useCallback(
  async (oldP1KeyShare: any, oldP2KeyShare: any): Promise<KeygenPair> => {
    try {
      const SESSION_ID = 'keygen_session_id';

      const p1KeyGen = await p1GetInstanceForKeyRefresh(
        SESSION_ID,
        JSON.stringify(oldP1KeyShare)
      );
      let p1KeyGenStorage: string = JSON.stringify(p1KeyGen);

      const p2KeyGen = await p2GetInstanceForKeyRefresh(
        SESSION_ID,
        JSON.stringify(oldP2KeyShare)
      );
      let p2KeyGenStorage: string = JSON.stringify(p2KeyGen);

      let p1KeyShare;
      let p2KeyShare;
      let p1KeyGenCompleted = false;
      let p2KeyGenCompleted = false;
      let messageFromParty1 = null;
      let messageFromParty2 = null;

      let tempP1KeyGen: IP1KeyGen;
      let tempP2KeyGen: IP2KeyGen;

      tempP1KeyGen = await p1KeyGenFromJSON(p1KeyGenStorage);
      const message1Result = await getP1KeyGenMessage1(
        JSON.stringify(tempP1KeyGen)
      );
      messageFromParty1 = message1Result.message1;
      p1KeyGenStorage = JSON.stringify(message1Result.p1KeyGen);

      // console.log('Silentshard', messageFromParty1);
      for (let i = 0; i < 2; i++) {
        if (!p2KeyGenCompleted) {
          tempP2KeyGen = await p2KeyGenFromJSON(p2KeyGenStorage);
          const p2KeyGenResult: IP2KeyGenResult = await p2KeyGenProcessMessage(
            JSON.stringify(tempP2KeyGen),
            messageFromParty1
          );
          p2KeyGenStorage = JSON.stringify(p2KeyGenResult.p2KeyGen);

          if (p2KeyGenResult.msg_to_send) {
            messageFromParty2 = p2KeyGenResult.msg_to_send;
            // console.log('Silentshard', messageFromParty2);
          }
          if (p2KeyGenResult.p2_key_share) {
            p2KeyShare = p2KeyGenResult.p2_key_share;
            p2KeyGenCompleted = true;
          }
        }

        if (messageFromParty2 && !p1KeyGenCompleted) {
          tempP1KeyGen = await p1KeyGenFromJSON(p1KeyGenStorage);
          const p1KeyGenResult = await p1KeyGenProcessMessage(
            JSON.stringify(tempP1KeyGen),
            messageFromParty2
          );

          p1KeyGenStorage = JSON.stringify(p1KeyGenResult.p1KeyGen);
          if (p1KeyGenResult.msg_to_send) {
            messageFromParty1 = p1KeyGenResult.msg_to_send;
            // console.log('Silentshard', messageFromParty1);
          }
          if (p1KeyGenResult.p1_key_share) {
            p1KeyShare = p1KeyGenResult.p1_key_share;
            p1KeyGenCompleted = true;
          }
        }
      }
      if (!(p1KeyShare && p2KeyShare)) {
        throw new Error('Keygen failed');
      }
      return { p1KeyShare, p2KeyShare };
    } catch (e) {
      console.log(e);
      throw e;
    }
  },
  []
);
```
