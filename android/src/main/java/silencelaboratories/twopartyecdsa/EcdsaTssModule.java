package silencelaboratories.twopartyecdsa;

import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.module.annotations.ReactModule;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import silencelaboratories.twopartyecdsa.keygen.KeyGenFailed;
import silencelaboratories.twopartyecdsa.keygen.P1KeyGen;
import silencelaboratories.twopartyecdsa.keygen.P2KeyGen;
import silencelaboratories.twopartyecdsa.signature.P1Signature;
import silencelaboratories.twopartyecdsa.signature.P2Signature;
import silencelaboratories.twopartyecdsa.signature.SignFailed;
import silencelaboratories.twopartyecdsa.utils.Pair;

@ReactModule(name = EcdsaTssModule.NAME)
public class EcdsaTssModule extends ReactContextBaseJavaModule {
    public static final String NAME = "EcdsaTss";

    public EcdsaTssModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    public byte[] randomBytes(int n) {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[n];
        random.nextBytes(bytes);
        return bytes;
    }

    @ReactMethod
    public void generateP1KeyGen(String sessionId, String x1, Promise promise) {
        byte[] xArray = null;
        if (x1 != null) {
            xArray = Base64.decode(x1, Base64.NO_WRAP);
        }
        try {
            P1KeyGen p1KeyGen = new P1KeyGen(sessionId, xArray);
            p1KeyGen.init();
            String p1KeyGenJSON = p1KeyGen.toJSONObject().toString();
            promise.resolve(p1KeyGenJSON);
        } catch (KeyGenFailed | JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p1KeyGenFromJSON(String p1KeyGenJSON, Promise promise) {
        try {
            P1KeyGen p1KeyGen = P1KeyGen.fromJSONObject(new JSONObject(p1KeyGenJSON));
            promise.resolve(p1KeyGen.toJSONObject().toString());
        } catch (JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void getP1KeyGenMessage1(String p1KeyGenJSON, Promise promise) {
        try {
            P1KeyGen p1KeyGen = P1KeyGen.fromJSONObject(new JSONObject(p1KeyGenJSON));
            String message1 = p1KeyGen.getMessage1().toString();
            WritableMap result = new WritableNativeMap();
            result.putString("p1KeyGen", p1KeyGen.toJSONObject().toString());
            result.putString("message1", message1);
            promise.resolve(result);
        } catch (KeyGenFailed | JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p1KeyGenProcessMessage(String p1KeyGenJSON, String msgFromParty2, Promise promise) {
        try {
            P1KeyGen tempP1KeyGen = P1KeyGen.fromJSONObject(new JSONObject(p1KeyGenJSON));
            Pair<JSONObject, P1KeyShare> pair = tempP1KeyGen.processMessage(new JSONObject(msgFromParty2));
            WritableMap result = new WritableNativeMap();
            result.putString("p1KeyGen", tempP1KeyGen.toJSONObject().toString());
            if (pair.getFirst() != null) {
                result.putString("msg_to_send", pair.getFirst().toString());
            }
            if (pair.getSecond() != null) {
                result.putString("p1_key_share", pair.getSecond().toJSONObject().toString());
            }
            promise.resolve(result);
        } catch (JSONException | KeyGenFailed e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void generateP2KeyGen(String sessionId, String x2, Promise promise) {
        byte[] xArray = null;
        if (x2 != null) {
            xArray = Base64.decode(x2, Base64.NO_WRAP);
        }
        try {
            P2KeyGen p2KeyGen = new P2KeyGen(sessionId, xArray);
            String p2KeyGenJSON = p2KeyGen.toJSONObject().toString();
            promise.resolve(p2KeyGenJSON);
        } catch (KeyGenFailed | JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p2KeyGenFromJSON(String p2KeyGenJSON, Promise promise) {
        try {
            P2KeyGen p2KeyGen = P2KeyGen.fromJSONObject(new JSONObject(p2KeyGenJSON));
            promise.resolve(p2KeyGen.toJSONObject().toString());
        } catch (JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p2KeyGenProcessMessage(String p2KeyGenJSON, String msgFromParty1, Promise promise) {
        try {
            P2KeyGen tempP2KeyGen = P2KeyGen.fromJSONObject(new JSONObject(p2KeyGenJSON));
            Pair<JSONObject, P2KeyShare> pair = tempP2KeyGen.processMessage(new JSONObject(msgFromParty1));
            WritableMap result = new WritableNativeMap();
            result.putString("p2KeyGen", tempP2KeyGen.toJSONObject().toString());
            if (pair.getFirst() != null) {
                result.putString("msg_to_send", pair.getFirst().toString());
            }
            if (pair.getSecond() != null) {
                result.putString("p2_key_share", pair.getSecond().toJSONObject().toString());
            }
            promise.resolve(result);
        } catch (KeyGenFailed | JSONException e) {
            promise.reject(e);
        }
    }

    // randBytes
    @ReactMethod
    public void getRandomBase64(int byteLength, Promise promise) {
        byte[] data = new byte[byteLength];
        SecureRandom random = new SecureRandom();
        random.nextBytes(data);
        promise.resolve(Base64.encodeToString(data, Base64.NO_WRAP));
    }

    // for the sign process p1
    @ReactMethod
    public void generateP1Signature(String sessionId, String messageHash, String p1KeyShareJSON, Promise promise) {
        try {
            byte[] messageHashBytes = Base64.decode(messageHash, Base64.NO_WRAP);
            P1Signature p1Signature = new P1Signature(sessionId, messageHashBytes, P1KeyShare.fromJSONObject(new JSONObject(p1KeyShareJSON)));
            String p1KeyGenJSON = p1Signature.toJSONObject().toString();
            promise.resolve(p1KeyGenJSON);
        } catch (JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p1SignatureFromJSON(String p1SignJSON, Promise promise) {
        try {
            P1Signature p1Signature = P1Signature.fromJSONObject(new JSONObject(p1SignJSON));
            promise.resolve(p1Signature.toJSONObject().toString());
        } catch (JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void getP1SignatureMessage1(String p1SignJSON, Promise promise) {
        try {
            P1Signature p1Sign = P1Signature.fromJSONObject(new JSONObject(p1SignJSON));
            String message1 = p1Sign.getMessage1().toString();
            WritableMap result = new WritableNativeMap();
            result.putString("p1Sign", p1Sign.toJSONObject().toString());
            result.putString("message1", message1);
            promise.resolve(result);
        } catch (SignFailed | JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p1SignatureProcessMessage(String p1SignJSON, String msgFromParty2, Promise promise) {
        try {
            P1Signature p1Sign = P1Signature.fromJSONObject(new JSONObject(p1SignJSON));
            Pair<JSONObject, String> pair = p1Sign.processMessage(new JSONObject(msgFromParty2));
            WritableMap result = new WritableNativeMap();
            result.putString("p1Sign", p1Sign.toJSONObject().toString());
            if (pair.getFirst() != null) {
                result.putString("msg_to_send", pair.getFirst().toString());
            }
            if (pair.getSecond() != null) {
                result.putString("signature", pair.getSecond());
            }
            promise.resolve(result);
        } catch (SignFailed | JSONException e) {
            promise.reject(e);
        }
    }

    // p2
    @ReactMethod
    public void generateP2Signature(String sessionId, String messageHash, String p2KeyShareJSON, Promise promise) {
        try {
            byte[] messageHashBytes = Base64.decode(messageHash, Base64.NO_WRAP);
            P2Signature p2Signature = new P2Signature(sessionId, messageHashBytes, P2KeyShare.fromJSONObject(new JSONObject(p2KeyShareJSON)));
            String p1KeyGenJSON = p2Signature.toJSONObject().toString();
            promise.resolve(p1KeyGenJSON);
        } catch (JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p2SignatureFromJSON(String p2SignJSON, Promise promise) {
        try {
            P2Signature p2Signature = P2Signature.fromJSONObject(new JSONObject(p2SignJSON));
            promise.resolve(p2Signature.toJSONObject().toString());
        } catch (JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p2SignatureProcessMessage(String p2SignJSON, String msgFromParty1, Promise promise) {
        try {
            P2Signature p2Sign = P2Signature.fromJSONObject(new JSONObject(p2SignJSON));
            Pair<JSONObject, String> pair = p2Sign.processMessage(new JSONObject(msgFromParty1));
            WritableMap result = new WritableNativeMap();
            result.putString("p2Sign", p2Sign.toJSONObject().toString());
            if (pair.getFirst() != null) {
                result.putString("msg_to_send", pair.getFirst().toString());
            }
            if (pair.getSecond() != null) {
                result.putString("signature", pair.getSecond());
            }
            promise.resolve(result);
        } catch (SignFailed | JSONException e) {
            promise.reject(e);
        }
    }

    // Key refresh
    @ReactMethod
    public void p1GetInstanceForKeyRefresh(String sessionId, String oldP1KeyShareJSON, Promise promise) {
        try {
            P1KeyGen p1KeyGen = P1KeyGen.getInstanceForKeyRefresh(sessionId, P1KeyShare.fromJSONObject(new JSONObject(oldP1KeyShareJSON)));
            p1KeyGen.init();
            String p1KeyGenJSON = p1KeyGen.toJSONObject().toString();
            promise.resolve(p1KeyGenJSON);
        } catch (KeyGenFailed | JSONException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void p2GetInstanceForKeyRefresh(String sessionId, String oldP2KeyShareJSON, Promise promise) {
        try {
            P2KeyGen p2KeyGen = P2KeyGen.getInstanceForKeyRefresh(sessionId, P2KeyShare.fromJSONObject(new JSONObject(oldP2KeyShareJSON)));
            String p2KeyGenJSON = p2KeyGen.toJSONObject().toString();
            promise.resolve(p2KeyGenJSON);
        } catch (KeyGenFailed | JSONException e) {
            promise.reject(e);
        }
    }
}
