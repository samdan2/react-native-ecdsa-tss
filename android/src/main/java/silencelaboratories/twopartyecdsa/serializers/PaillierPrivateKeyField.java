package silencelaboratories.twopartyecdsa.serializers;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;

import silencelaboratories.twopartyecdsa.phe.PaillierPrivateKey;
import silencelaboratories.twopartyecdsa.phe.PaillierPublicKey;


public class PaillierPrivateKeyField {

    public static JSONObject serialize(PaillierPrivateKey data) throws JSONException {
        BigInteger p = data.getP();
        BigInteger q = data.getQ();
        JSONObject m = new JSONObject();
        m.put("p", PosBigIntegerField.toBase64(p));
        m.put("q", PosBigIntegerField.toBase64(q));
        return m;
    }

    public static PaillierPrivateKey deserialize(JSONObject m) throws JSONException {
        BigInteger p = PosBigIntegerField.fromBase64((String) m.get("p"));
        BigInteger q = PosBigIntegerField.fromBase64((String) m.get("q"));
        BigInteger n = p.multiply(q);
        return new PaillierPrivateKey(new PaillierPublicKey(n), p, q);
    }

}
