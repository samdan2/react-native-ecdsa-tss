const path = require('path');

module.exports = {
  dependencies: {
    'react-native-ecdsa-tss': {
      root: path.join(__dirname, '..'),
    },
  },
};
